class AddRootCauseToIssues < ActiveRecord::Migration
    def change
      add_column :issues, :root_cause, :string
      add_index :issues, :root_cause
    end
  end