class AddExternalRefrestToIssues < ActiveRecord::Migration
    def change
      add_column :issues, :external_ref5, :string
      add_index :issues, :external_ref5
      add_column :issues, :external_ref6, :string
      add_index :issues, :external_ref6
      add_column :issues, :external_ref7, :string
      add_index :issues, :external_ref7
      add_column :issues, :external_ref8, :string
      add_index :issues, :external_ref8
      add_column :issues, :external_ref9, :string
      add_index :issues, :external_ref9
      add_column :issues, :external_refa, :string
      add_index :issues, :external_refa
      add_column :issues, :external_refb, :string
      add_index :issues, :external_refb
      add_column :issues, :external_refc, :string
      add_index :issues, :external_refc
      add_column :issues, :external_refd, :string
      add_index :issues, :external_refd
    end
  end