class AddExternalRefToIssues < ActiveRecord::Migration
  def change
    add_column :issues, :external_ref, :string
    add_index :issues, :external_ref
  end
end
