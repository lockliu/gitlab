class AddExternalRef2ToIssues < ActiveRecord::Migration
    def change
      add_column :issues, :external_ref2, :string
      add_index :issues, :external_ref2
    end
  end