class AddExternalRef34ToIssues < ActiveRecord::Migration
    def change
      add_column :issues, :external_ref3, :string
      add_index :issues, :external_ref3
      add_column :issues, :external_ref4, :string
      add_index :issues, :external_ref4
    end
  end